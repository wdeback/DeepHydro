# -*- coding: utf-8 -*-
"""
Created on Thu Sep 12 15:01:59 2019

@author: vinograd
"""
Debug=True#False

import os
import random
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
    
from keras import backend as K
import cv2
from tqdm import tqdm_notebook as tqdm 

def min_mean_max_sum(arr):
    return np.min(arr), np.mean(arr), np.max(arr), np.sum(arr)
def plot_saliency_return_stats(image, shap_values, mask,save=''):
    """ Return statistics of the saliencies and the data for each frame.
    Stats are returned as a pandas DataFrame. 
    Stats can be useful to find correlations between 
    e.g. maximal saliency across temperature image and average temperature for day X"""
    
    n_frames = len(image)
    ink=5
    fig, ax = plt.subplots(n_frames, 4, figsize=(4*ink, n_frames*ink)) 
    stats = np.zeros((n_frames,4*4))
    vmin_sal = np.min( shap_values )
    vmax_sal = np.max( shap_values )
    vmin_t = np.min( image[..., 0] )
    vmax_t = np.max( image[..., 0] )
    vmin_p = np.min( image[..., 1] )
    vmax_p = np.max( image[..., 1] )
    for frame, (im, sal)  in tqdm(enumerate(zip(image, shap_values)), total=len(image)):

        vmin = np.min( sal )
        vmax = np.max( sal )
        im_t = np.ma.masked_array(sal[..., 0], mask=mask)
        #t_sal_max,t_sal_mean,t_sal_sum
        stats[frame,:4]= min_mean_max_sum(im_t)
        #np.around(np.max(im_t),decimals=4)
        ax[frame,0].set_title(f'Saliency temp day {frame} \n MaxSaliency {np.around(np.max(im_t),decimals=4)}')
        ax0_show=ax[frame,0].imshow(im_t, vmin=vmin_sal, vmax=vmax_sal)
        ax[frame,0].axis('off')
        fig.colorbar(ax0_show,ax=ax[frame,0])

        ax[frame,1].set_title(f'Temp day {frame}')
        vmin = np.min( im )
        vmax = np.max( im )
        im_t = np.ma.masked_array(im[..., 0], mask=mask)
        ax1_show = ax[frame,1].imshow(im_t, vmin=vmin_t, vmax=vmax_t)
        ax[frame,1].axis('off')
        fig.colorbar(ax1_show,ax=ax[frame,1])
        stats[frame,4:8]= min_mean_max_sum(im_t)

    
        vmin = np.min( sal )
        vmax = np.max( sal )
        im_t = np.ma.masked_array(sal[..., 1], mask=mask)
        im_t_sal1_max=np.around(np.max(im_t),decimals=4)
        ax[frame,2].set_title(f'Saliency prec day {frame} \n MaxSaliency {im_t_sal1_max}')
        ax2_show=ax[frame,2].imshow(im_t, vmin=vmin_sal, vmax=vmax_sal)
        ax[frame,2].axis('off')
        fig.colorbar(ax2_show,ax=ax[frame,2])
        stats[frame,8:12]= min_mean_max_sum(im_t)

        ax[frame,3].set_title(f'Prec day {frame}')
        vmin = np.min( im )
        vmax = np.max( im )
        im_t = np.ma.masked_array(im[..., 1], mask=mask)
        ax3_show=ax[frame,3].imshow(im_t, vmin=vmin_p, vmax=vmax_p)
        ax[frame,3].axis('off')
        fig.colorbar(ax3_show,ax=ax[frame,3])
        stats[frame,12:]= min_mean_max_sum(im_t)
    if len(save)>0:
        plt.savefig(save)
    plt.show()
    
    return pd.DataFrame(stats,columns=['min temp sal','mean temp sal','max temp sal','sum temp sal',
                                       'min temp','mean temp','max temp','sum temp',
                                     'min precip sal','mean precip sal','max precip sal','sum precip sal',
                                       'min precip','mean precip','max precip','sum precip'
                            ])
    
def plot_saliency_1frame_color(image, shap_values, mask):
    """ Rescale colorbars to min max of the saliency for 1 frame
    Return statistics of the saliencies and the data for each frame.
    Stats are returned as a pandas DataFrame. 
    Stats can be useful to find correlations between 
    e.g. maximal saliency across temperature image and average temperature for day X"""

    n_frames = len(image)
    fig, ax = plt.subplots(n_frames, 4, figsize=(2*6, n_frames*3)) 
    stats = np.zeros((n_frames,2*6))
    vmin_sal = np.min( shap_values )
    vmax_sal = np.max( shap_values )
    vmin_t = np.min( image[..., 0] )
    vmax_t = np.max( image[..., 0] )
    vmin_p = np.min( image[..., 1] )
    vmax_p = np.max( image[..., 1] )
    for frame, (im, sal)  in tqdm(enumerate(zip(image, shap_values)), total=len(image)):

        
        vmin = np.min( sal[..., 0] )
        vmax = np.max( sal[..., 0] )
        im_t = np.ma.masked_array(sal[..., 0], mask=mask)
        #t_sal_max,t_sal_mean,t_sal_sum
        stats[frame,:3]= max_mean_sum(im_t)
        #np.around(np.max(im_t),decimals=4)
        ax[frame,0].set_title(f'Saliency temp day {frame} \n MaxSal frame {np.around(np.max(im_t),decimals=4)}')
        ax0_show=ax[frame,0].imshow(im_t, vmin=vmin#_sal
                                    , vmax=vmax) #_sal)
        ax[frame,0].axis('off')
        fig.colorbar(ax0_show,ax=ax[frame,0])

        ax[frame,1].set_title(f'Temp day {frame}')
        vmin = np.min( im )
        vmax = np.max( im )
        im_t = np.ma.masked_array(im[..., 0], mask=mask)
        ax1_show = ax[frame,1].imshow(im_t, vmin=vmin_t, vmax=vmax_t)
        ax[frame,1].axis('off')
        fig.colorbar(ax1_show,ax=ax[frame,1])
        stats[frame,3:6]= max_mean_sum(im_t)

    
        vmin = np.min( sal[..., 1] )
        vmax = np.max( sal[..., 1] )
        im_t = np.ma.masked_array(sal[..., 1], mask=mask)
        im_t_sal1_max=np.around(np.max(im_t),decimals=4)
        ax[frame,2].set_title(f'Saliency prec day {frame} \n MaxSal frame {im_t_sal1_max}')
        ax2_show=ax[frame,2].imshow(im_t, vmin=vmin#_sal
                                    , vmax=vmax) #_sal)
        ax[frame,2].axis('off')
        fig.colorbar(ax2_show,ax=ax[frame,2])
        stats[frame,6:9]= max_mean_sum(im_t)

        ax[frame,3].set_title(f'Prec day {frame}')
        vmin = np.min( im )
        vmax = np.max( im )
        im_t = np.ma.masked_array(im[..., 1], mask=mask)
        ax3_show=ax[frame,3].imshow(im_t, vmin=vmin_p, vmax=vmax_p)
        ax[frame,3].axis('off')
        fig.colorbar(ax3_show,ax=ax[frame,3])
        stats[frame,9:]= max_mean_sum(im_t)
    
    plt.show()
    
    return pd.DataFrame(stats,columns=['max temp sal','mean temp sal','sum temp sal','max temp','mean temp','sum temp',
                             'max precip sal','mean precip sal','sum precip sal','max precip','mean precip','sum precip'
                            ])

def grad_convlstm(input_model, image,predictions, 
                         #prop_from_layer,
                         prop_to_layer='conv_lst_m2d_2', 
                         normalize=True,abs_w=False,posit_w=False):
    """GradCAM that works for regression output & multidim input for lstm model """
    print(image.shape)
    preprocessed_input = np.expand_dims(image, 0)#load_image(img_path)
    print("preprocessed_input.shape ",preprocessed_input.shape)
    #img = np.expand_dims(img, 0)
    #predictions = input_model.predict(preprocessed_input)
    #print(predictions)
    #predicted_class = predictions    
    prop_from_layer = input_model.layers[-1].name #last dense
    #prop_to_layer = input_model.layers[3].name #first convlstm | last = -4
    print("prop_to_layer ",prop_to_layer)
    y_c = input_model.get_layer(prop_from_layer).output#[...,i,j,cls]
    conv_output = input_model.get_layer(prop_to_layer).output
    grads = K.gradients(y_c, conv_output)[0]
    """
    Arguments:
    loss: Scalar tensor to minimize.
    variables: List of variables.
    Returns: A gradients tensor."""
    # Normalize if necessary
    # grads = normalize(grads)
    gradient_function = K.function([input_model.get_input_at(0)[0]#input#get_input_at(0) for models with more inputs than just an image
                                   ], [conv_output, grads])
    """The part of code snippet is as follows -

    final_conv_layer = get_output_layer(model, "conv5_3")
    get_output = K.function([model.layers[0].input], [final_conv_layer.output, model.layers[-1].output])
    [conv_outputs, predictions] = get_output([img])
In this code, there is a model from which "conv5_3" layer is extracted (line 1). 
iN the function K.function(), first argument is input to this model and second is set of 2 
outputs - one for convolution and second for softmax output at the last layer.

As per the Keras/Tensorflow manual, this function runs the computation graph that we have created 
in the code, taking input from the first parameter and extracting the number of outputs as per the
 layers mentioned in the second parameter. Thus, conv_outputs are output of final_conv_layer and 
 predictions are output of model.layers[-1] i.e. last layer of the model.
    """
    output, grads_val = gradient_function([preprocessed_input])
    print("output, grads_val shapes: ",output.shape, grads_val.shape)
    output, grads_val = output[0, :], grads_val[0, :, :, :]
    print("output, grads_val shapes: ",output.shape, grads_val.shape)
    #print("output[0, :], grads_val[0, :, :, :] ; ",output, grads_val)
    weights = np.mean(grads_val, axis=(0, 1,2))#,3)) # here's the change for our multidim images
    
    if abs_w:
        weights = abs(weights)
    if posit_w:
        weights=np.maximum(weights,0)
        
    cam = np.dot(output, weights) #*abs(grads_val) or max(grads_val,0)
    img_dim = image.shape[1:3] #works for all convlstm layers except the last since it has no temporal dim
    if Debug:
        print("image shape: ", image.shape)
        print("cam shape: ", cam.shape)
        print("img_dim: ",img_dim)
        #print("input_model.output ",input_model.output)#str(K.eval(input_model.output))) #str(y_true.eval())
        #print("y_c: ", y_c)
        #print("conv_output: ",conv_output)
        #print("K.gradients(y_c, conv_output) ", K.gradients(y_c, conv_output))
        #print("K.gradients(y_c, conv_output)[0] aka grads: ", grads)
        #print("gradient_function: ",gradient_function)
        print("output.shape, grads_val.shape: ",output.shape, grads_val.shape)
        #print("output, grads_val: ",output, grads_val)
        print("weights.shape : ",weights.shape)
    # Process CAM
    print("cam T shape: ", cam.T.shape)
    unresizedcam=cam
    cam = cv2.resize(cam.T, img_dim)#[::-1])#, cv2.INTER_LINEAR) #cv2.resize(image,(width,height))
    print("resized cam shape: ", cam.shape)
    cam = np.maximum(cam, 0)
    cam_max = cam.max() 
    if cam_max != 0 and normalize: 
        cam = cam / cam_max
    return cam,predicted_class, unresizedcam

def avg_and_max_time_frames(gradcam,mask,layer):
    # Example:avg_and_max_time_frames(gradcam_2lstm,mask,layer='2nd convLSTM') 
    fig, ax = plt.subplots(1, 2, figsize=(20,10))#,squeeze=False)
    ax[0].set_title('Max saliencies \nby Grad-CAM at '+layer)
    vmin_sal =np.min(gradcam[0].T)
    vmax_sal =np.max(gradcam[0].T)
    # max across frames
    gradcam_max = np.max(gradcam[0].T, axis=0)
    print(gradcam_max.shape)
    mask_gradcam_max = np.ma.masked_array(gradcam_max, mask=mask)
    ax_0=ax[0].imshow(mask_gradcam_max, vmin=vmin_sal
                                    , vmax=vmax_sal)
    fig.colorbar(ax_0,ax=ax[0],fraction=0.046, pad=0.04)
    # avg across frames
    ax[1].set_title('Saliencies by Grad-CAM at '+layer+'\n averaged across frames')
    gradcam_avg = np.mean(gradcam[0].T, axis=0)
    print(gradcam_avg.shape)
    mask_gradcam_avg = np.ma.masked_array(gradcam_avg, mask=mask)
    ax_1=ax[1].imshow(mask_gradcam_avg, vmin=vmin_sal
                                    , vmax=vmax_sal)
    fig.colorbar(ax_1#,ax=ax[1]
                 ,fraction=0.046, pad=0.04)
    
