### To Dos

1. [ ] Gather literature (Lennart)
2. [x] Clean up repo (unused files, large files, consistent naming of models...)
3. [ ] Create model keeping routine (Kira)
4. [x] List Applied ML-conferences (Kira)
4. [x] Set-up Large-File Storage (Lennart)
2. [ ] Train both on "start", "middle", "end"-training sets
	1. [ ] middle = test both at beginning and end, ca. 210 datapoints lost as buffer
	2. [ ] Compare temporal dynamics and _test accuracy_ with mHm
	3. [ ] Compare temporal dynamics and _test accuracy_ with LSTM
	4. [ ] Decide which splitsampling to use
3. [ ] Change evaluation metric to KGE (Lennart)
2. [ ] Error analysis (Elona)
2. [ ] Improve ConvLSTM-accuracy
	1. [ ] Less Conv-Layers (Lennart)
	2. [ ] Investigate optimal sequence length in LSTM (Elona)
	3. [ ] Two-step learning (CNN-LSTM) (Kira)
	4. [ ] Systematic Hyperparameter Tuning (CV)
	5. [ ] Add air pressure as input to account for type of rainfall (Lennart)
3. Saliency maps
    1. [ ] Derive averaged saliency maps and temporal importances for flood events
    2. [ ] Normalize saliency maps by precipitation
4. [ ] Analyze why spatial distribution of inputs leads to such small gains in accuracy
	1. [ ] Upscale inputs to coarser resolution, retrain and compare


#### Add-On
1.
2. [ ] Stateful-LSTM structure

4. [ ] Salinency maps of models that include uncertainty (?)