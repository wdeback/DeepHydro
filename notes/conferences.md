| Name: | Deadline: | Num pages: | Link: | Where | When | Add-ons |       

| KDD | February 13, 2020 | 9 | https://www.kdd.org/kdd2020/calls/view/kdd-2020-call-for-research-papers | San Diego, US | August 22-27, 2020 | scientific and business data, time series,  spatio-temporal data, interpretability |    

| UAI | February 20th, 2020 | tba | http://auai.org/uai2020/subject_areas.php | Toronto, Canada | August 3rd - August 6th, 2020 | Uncertainty in Artificial Intelligence, a track for us - Sustainability and Climate |     

| ECCV | 5 March 2020 | tba | https://eccv2020.eu/important-dates/ |Glasgow, Scotland | 23-28 August 2020 | well-known CV conference with a wide range of topics |  

| NeurIPS | ~15 May 2020 | 9 | https://nips.cc/Conferences/FutureMeetings | Vancouver, Canada | Dec 2020 | climate topics. Top conference. Super-low acceptance rate |