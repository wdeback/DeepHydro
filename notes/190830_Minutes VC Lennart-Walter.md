## Minutes VC Lennart-Walter 30.08.2019

#### General

* Walter approves the approach in general
* See "Pitch.pptx" for a list of the open questions that we discussed
* Focus during Hackathon: Knowledge gain by applying Layerwise Relevance Propagation or similar method. Other research ideas later (see slides)


#### Data
* Data is more coarse than expected, which is helpful in Walter's eyes (less RAM-issues)
* Lennart tried to get hold on finer resolution but there is none that is useful for us
* Still, ~25.000 images (x2 bands) is enough to train
* Idea from Walter: Why not use more than one gauging station in the catchment as target? More information to learn from. --> Lennart will organise more data
* Merging precipitation and temperature data: 2-band stacked numpy array


#### Model structure

* We don't have to worry about how to combine CNN+LSTM, it's a all-in-one structure already implemented in Keras
* No pretraining necessary or useful
* At first Walter was not sure whether we need the LSTM, but as time lags can be up to 1-2 years it is most probably the best structure
* Walter has some experience with CNN-LSTMs
* Open question: Possible to extract the long- and short term signal that was fitted by the model?

#### Layer-wise Relevance Propagation

* Walter is not particularly fond of these methods, tend to give messy results
* Alternative approaches (exact names will follow):


**Deep Lift:**

* Walter has applied this recently
* Saliency maps are calculated for bins of the target variable, i.e. based on ~50 input images
* These are compared against a reference fo the whole timeseries, one final output saliency map is calculated by averaging. Variance of those 50 images can be used to estimate uncertainty of final map
* Assumes normal distribution (Alternative: shapley-method)
* Probably this is the approach that we will use
* Unclear whether it can be easily be applied to LSTM structure to give time-lagged saliency maps, i.e. of preconditions

**SHAP**

* Based on shapley values
* Similar but based on the data's distribution, not normal
* [github](deepstop divecenter)

**Intergrated gradients, DeepTaylor...**

* Packages that all contain multiple methods: Innvestigate, DeepShap

#### Outlook
* Walter: Provide example jupyter notebook
* Walter: Check if he has good literature on CNN-LSTMs
* Walter: Read whether it is possible to extract long- and shortterm signals to compare it with baseflow/direct runoff
* Lennart: Get data of other stations in the Elbe catchment
* Lennart: Check whether there is finer resolution data (no)
* Lennart: How many stations are in the catchment?
* Lennart: Crop digital elevation model
