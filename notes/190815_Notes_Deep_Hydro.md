## Notes Deep Hydro

### To Dos

1. [ ] Set up venv with keras+jupyterlab for testing (Elona)
2. [x] Clip data from rasters to catchment of gauge (*Lennart, Rohini*) 
3. [x] Clip Digital elevation model (*Lennart*)
4. [ ] Prepare data from other stations within Elbe catchment
5. [x] Get output from physical model run (Baseflow, direct runoff, snow cover...)
6. [x] Upload data to Taurus + become familiar
2. [ ] Data Analysis
	1. [x] Missing values
	2. [ ] Plot yearly P+T patterns (mean+coefficient of variation)
	3. [x] Yearly summary statistics of discharge (histogram, mean/cv...)
3. [ ] Preprocessing
	1. [x] Drop missing values
	2. [x] Normalize data
	2. [x] Transfer into tensorflow input format
	3. [x] Calculate flood recurrence intervals (*Lennart*)
4. [x] First Model fitting
5. [ ] Hyperparameter-tuning
5. [ ] Evaluation
	1. [ ] Output plotting routines
6. [ ] Analysis
	1. [ ] LRP/Deep Lift


### Necessary knowledge

1. [ ] Basics CNN (*Elona, Lennart*)
2. [ ] Basics LSTM (*Elona, Lennart*)
	1. [ ] Memory/forget gates
	2. [ ] Get 3D-visualization (*Lennart*)
2. [ ] Pitfalls in training Deep Networks (*Elona*)
3. [ ] Packages for bayesian Hyperparameter-tuning (*Elona*)
4. [ ] Method of "Deep Lift" / "Shapley"
5. [ ] Domain knowledge: Mechanisms of extreme events Elbe (Lennart/Rohini)
6. [ ] How many climate stations are inside the catchment? (*Lennart*)
7. [ ] Is DeepList applicable to LSTM (*Walter*)
8. [ ] Extraction of long/short-term component to compare against baseflow/direct runoff


### Infrastructure
* GitLab
* Which server to use?
* Scripts or Jupyter Notebooks?











### Data


#### E-OBS data:
* Interpolated maps of precipitation and temperature from ensemble models
* NetCDF raster format
* In Germany based on 262 meteorological stations
* in the Elbe catchment: 81
* Spatial resolution: 0.1°, i.e. +-6km
* Temporal resolution: Daily
* Time-period: 01.01.1950-31.12.2018, no gaps

#### Discharge data:
* mean daily discharge in m^3/s of Elbe river at gauge "Neu Darchau"
* Time period: 1874 - 2016
* Stations available in catchment: 70 (not all same coverage