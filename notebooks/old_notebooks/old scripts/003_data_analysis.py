import matplotlib.pyplot as plt

import numpy as np
import os, sys
import pandas as pd
from matplotlib.backends.backend_pdf import PdfPages

from scipy import stats

# set console output
desired_width=640
pd.set_option('display.width', desired_width)
np.set_printoptions(linewidth=desired_width)
pd.set_option('display.max_columns',10)



os.chdir("/Users/schmidle/Documents/GIT-Projects/DeepHydro")
data_folder = 'Data/'
#data_folder = '../Datasample'
data_prec = '01_prec.nc'
data_temp = '01_temp.nc'
data_disc = '01_discharge.csv'

def read_output_data(data_folder:str, data_disc:str):
    # read csv file
    df = pd.read_csv(os.path.join(data_folder, data_disc))
    # calculate the difference to midsummer
    df['DOY'] = pd.DatetimeIndex(df['Time']).dayofyear
    # calculate the difference to midsummer
    df['DiffToMidSummer'] = abs( pd.DatetimeIndex(df['Time']).dayofyear - 365/2)
    return df

df = read_output_data(data_folder, data_disc)


#################################
########### Analysis of streamflow
#################################
df.shape #24472 entries
### NAs?
sum(df.Q.isna()) # none

df.Time = pd.to_datetime(df.Time)
df = df.set_index(df.Time)


### Plot yearly histograms of Q
i=1
years = df.Time.dt.year.unique()
plotbins = np.histogram(df.Q,40)[1]
pp = PdfPages("Plots/003_QHists.pdf")
for h in range(4):
    start = h*20
    end = ((h+1)*20)
    years2 = years[start:end]
    #print(str(min(years2)) + " "+ str(max(years2)))
    #fig, ax = plt.subplots(5,4,figsize=(15,15))
    fig = plt.figure(figsize=(15,15))
    for i in range(len(years2)):
        #plt.subplot(5, 4, i+1)
        #plt.hist(df.Q[df.Time.dt.year==years2[i]],bins=plotbins)
        ax=fig.add_subplot(5,4,i+1)
        ax.hist(df.Q[df.Time.dt.year==years2[i]],bins=plotbins)
        ax.set_title(years2[i])
    plt.tight_layout()
    pp.savefig()
    plt.close()
pp.close()

#################################
########### Analysis of precipitation and temperature
#################################

def read_data(filename, folder, varname):
    import netCDF4
    dat = netCDF4.Dataset(os.path.join(folder, filename))
    print(filename)
    arr = np.array(dat.variables[varname])
    mask = arr < -100
    arr[ mask ] = 0.0
    return arr, mask

temp, tempmask = read_data(data_temp, data_folder,varname='z (unknown)')
prec, precmask = read_data(data_prec, data_folder,varname='z (unknown)')

tempmask[1]

### NAs? Derive from maks which is True if input data <0, i.e. -3.4e+38
def countnas (inputmask):
    out=[]
    for i in range(inputmask.shape[0]):
        out.append(np.sum(inputmask[i]))
    print("Unique values of mask=True: "+str(np.unique(out)))
    return(np.array(out))

countprec = countnas(precmask)#They all contain 1821 NAs, so ok
counttemp = countnas(tempmask)#ok
