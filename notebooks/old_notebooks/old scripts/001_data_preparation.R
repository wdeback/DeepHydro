# to be run on cluster!

#local:
setwd("~/Documents/GIT-Projects/DeepHydro")

library("raster")
library("ncdf4")
library("rgdal")
library("lubridate")
library("stringr")
library(snow)



###############################################################
### Set raster settings for cluster usage
###############################################################
maxram = 6
rasterOptions(tmpdir="TEMP/",timer=T,maxmemory=134217728*maxram)#temp folder, maximum ram usage,chunksize=4gb

###############################################################
### Load data
###############################################################
### precipitation data
pdata <- brick("Data/Rawdata/EOBS complete/rr_ens_mean_0.1deg_reg_v19.0e.nc")

### temp data
tdata <- brick("Data/Rawdata/EOBS complete/tg_ens_mean_0.1deg_reg_v19.0e.nc")

### Load Elbe catchment shapefile
catchment = readOGR("Data/Rawdata/mask+dem/mask_shp/mask_6340110.shp")
### Save catchment as raster
#catchmentraster <- crop(pdata[[1]],catchment)#create output raster for extent, resolution etc whole extent, move this further
#catchmentraster <- rasterize(catchment,catchmentraster)
#writeRaster(catchmentraster,"Data/01_catchmentraster.tif",format="GTiff",overwrite=T)
#writeRaster(catchmentraster,"Data/01_catchmentraster.nc",format="CDF",overwrite=T)



### Load discharge data
discharge <-  read.table("Data/Rawdata/Discharge_GRDC.txt",header=T,comment.char="#",na.strings = "-999.000",sep=";")
discharge <- discharge[,-2]
names(discharge) <- c("Time","Q")
discharge$Time <- as_datetime(discharge$Time)#same time structure as other data

# ###############################################################
# ### standardize timeseries lengths
# ###############################################################
print("Starting cutting timeseries")
time_tp <- as_datetime(str_replace(names(pdata),"X",""))# time vector of raster data
keep_discharge <- which(discharge$Time%in%time_tp)#filter days that both have data
keep_tp <- which(time_tp%in%discharge$Time)
# drop unnneded data
discharge <- discharge[keep_discharge,]
pdata <- pdata[[keep_tp]]
tdata <- tdata[[keep_tp]]
print("Done")
#############################################################
### mask EOBS-rasters using a shapefile of the Elbe-catchment
###############################################################
# see here: https://stackoverflow.com/questions/50204653/how-to-extract-the-data-in-a-netcdf-file-based-on-a-shapefile-in-r
# and here: https://everydropr.wordpress.com/2012/09/22/how-to-efficiently-mask-raster-layer-in-r/
## Crop E-OBS to a smaller raster size
print("Starting cropping")
pdata_crop = crop(pdata,catchment)
#pdata_crop = clusterR(pdata,fun=crop,args=list(catchment))
print("P finished")
tdata_crop = crop(tdata,catchment)
#tdata_crop = clusterR(tdata,fun=crop,args=list(catchment))
print("T finished")

### Clip EOBS according to ELBE
beginCluster(4)
print("Starting masking")
#pdata_mask = mask(pdata, maskraster)
pdata_mask <- clusterR(pdata_crop,fun=mask,args=list(catchment))
print("P finished")
#tdata_mask = mask(tdata, maskraster)
tdata_mask <- clusterR(tdata_crop,fun=mask,args=list(catchment))
print("T finished")
endCluster()  

names(pdata_mask) <- as.character(discharge$Time)
names(tdata_mask) <- as.character(discharge$Time)

### Load,crop+upscale digital elevation model
dem <- raster("Data/Rawdata/mask+dem/dem.asc")
crs(dem) <- crs(pdata)#update projection info
dem <- crop(dem,pdata_mask[[1]])
# save at fine resolution
#riteRaster(dem,"Data/01_dem_highres.tif",format="GTiff",overwrite=T)
writeRaster(dem,"Data/01_dem_highres.nc",format="CDF",overwrite=T)

### Resample DEM to the right resolution
dem_resample <- resample(dem,pdata_mask[[1]])
dem_resample <- crop(dem_resample,pdata_mask[[1]])

plot(dem_resample)

### write files
print("Writing files")
write.csv(discharge,"Data/01_discharge_cropped.csv")
writeRaster(pdata_mask,"Data/01_prec_cropped.nc",format="CDF",overwrite=T)
writeRaster(tdata_mask,"Data/01_temp_cropped.nc",format="CDF",overwrite=T)

writeRaster(pdata_mask,"Data/01_prec_cropped.tif",format="GTiff",overwrite=T)
writeRaster(tdata_mask,"Data/01_temp_cropped.tif",format="GTiff",overwrite=T)

writeRaster(dem_resample,"Data/dem/01_dem.nc",format="CDF",overwrite=T)

# write as standard tif file
pdata_mask <- brick("Data/OLD/01_prec_cropped.nc")
writeRaster(pdata_mask,"Data/01_prec_rawtiff.tif",format="GTiff",overwrite=T,options=c("PROFILE=BASELINE") )
tdata_mask <- brick("Data/OLD/01_temp_cropped.nc")
writeRaster(tdata_mask,"Data/01_temp_rawtiff.tif",format="GTiff",overwrite=T,options=c("PROFILE=BASELINE") )

dem_resample <- raster("Data/dem/01_dem.nc")
writeRaster(dem_resample,"Data/01_dem_rawtiff.tif",fformat="GTiff",overwrite=T,options=c("PROFILE=BASELINE"))



<- brick("Data/OLD/01_prec_cropped.tif")
ras_prec

ras_temp <- brick("Data/OLD/01_temp_cropped.tif")
plot(ras_temp[[1]])
ras_temp

ras_prec
