library(ggplot2)
library(lubridate)
library(RColorBrewer)
### how many station in ger?
stations <- read.delim("Data/Rawdata/EOBS complete/stations_info_pp_v19.0e.txt",sep="|",header=T)
germanstations <- grep("GERMANY",stations$COUNTRY)
length(germanstations)#262

# load raster data
pdata <- brick("Data/01_prec_cropped.nc")
tdata <- brick("Data/01_temp_cropped.nc")

### Load Elbe catchment shapefile
catchment = readOGR("Data/Rawdata/mask+dem/mask_shp/mask_6340110.shp")

#extract subset
subset_size=3
pslice <- pdata[[1:subset_size]]
tslice <- tdata[[1:subset_size]]

# Plot difference between catchment-shapefile and clipped rasters
image(tslice[[1]])
plot(catchment,add=T, col="black")# does not look too bad

###########
### Raster plots
##########
png("Plots/sample_P.png",height=7,width=23,unit="cm",res=500)
plot(pslice,nc=subset_size,nr=1,col=c(brewer.pal(9,"Blues")[1:9]),colNA="gray95")#otional: zlim=c(0,25)
dev.off()
png("Plots/sample_T.png",height=7,width=23,unit="cm",res=500)
plot(tslice,nc=subset_size,nr=1,col=rev(heat.colors(20)),colNA="gray95")#optional: zlim=c(-9,5)
dev.off()

###########
### Plot High-Res Elevation
##########
dem <- raster("Data/Rawdata/mask+dem/dem.asc")
crs(dem) <- crs(pdata)#update projection info
dem[dem<10] <- -1 # Why are there negative values?

png("Plots/002_dem.png",height=14,width=14,unit="cm",res=800)
par(oma=rep(0,4),mar=c(2.1,2.1,1.1,3.1))
plot(dem,col=c("black",rev(terrain.colors(255))))
dev.off()
###########
### Timeseries plot of discharge
##########
discharge <-  read.csv("Data/01_discharge_cropped.csv")
discharge$Time <- as_datetime(discharge$Time)
plotdata <- discharge[year(discharge$Time)==2006&month(discharge$Time)%in%c(1:12),]

ggplot(data=plotdata)+
  geom_line(aes(x=Time,y=Q))+
  xlab("Time")+
  ylab(bquote(paste("Mean daily streamflow Q [",m^3,"/s]")))+
  theme_bw()
ggsave("Plots/002_timeseriesQ.pdf",device="pdf",width=4,height=3)
