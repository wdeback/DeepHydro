#os.chdir('/scratch/ws/gpu16-d3hack2019-DeepHydro/')
import numpy as np
import pandas as pd
import os, sys
import matplotlib.pyplot as plt

### import deephydro module
%load_ext autoreload
%autoreload 2
module_folder = os.path.abspath('src/')
#module_folder = 'src/'
if module_folder not in sys.path:
    sys.path.append(module_folder)
from deephydro import data, visualization

data_folder = '../Data/Npy'
data_disc = '01_discharge_training.csv'
data_prec = '01_prec_training.npy'
data_temp = '01_temp_training.npy'
data_elev = '01_dem.npy'
data_mask = '01_mask.npy'

df = data.load_csv_data(data_folder=data_folder, filename=data_disc)
df.head()
visualization.plot_streamflow(df=df)

prec = data.load_from_numpy(data_folder, filename=data_prec)
temp = data.load_from_numpy(data_folder, filename=data_temp)
mask = data.load_from_numpy(data_folder, filename=data_mask)
elev = data.load_from_numpy(data_folder, filename=data_elev)
# check dimensions
print(elev.shape)
print(temp.shape)
print(prec.shape)
print(mask.shape)

# Split datasets



# chop into test+training
timeseq = pd.date_range(start='1950-01-01', end='2016-12-31', freq='D')
len(timeseq)
chopdate = pd.to_datetime('2003-01-01')
testmask = timeseq>=chopdate
trainmask = timeseq<chopdate



# whats the share of test data?
sum(testmask)/len(testmask)*100#~20%

prec_tr = prec[trainmask]
prec_te = prec[testmask]
temp_tr = temp[trainmask]
temp_te = temp[testmask]


# # Define 15% validation from training data
# from deephydro.data import split_train_validation
# # stack inputs
# x = np.stack([temp, prec], axis=-1)
# print('Shape of x = ', x.shape)
# x_train, x_test, y_train, y_test = split_train_validation(x, df.Q ,validation_fraction=0.15,validation_first=False)


#save
np.save("Data/Npy/01_prec_training.npy",prec[trainmask])
np.save("Data/Npy/01_temp_training.npy",temp[trainmask])
np.save("Data/Npy/01_prec_test.npy",prec[testmask])
np.save("Data/Npy/01_temp_test.npy",temp[testmask])

df[trainmask].to_csv("Data/Npy/01_discharge_training.csv",index=False)
df[testmask].to_csv("Data/Npy/01_discharge_test.csv",index=False)


###############################
### Verification + Plotting
###############################

disch_tr = pd.read_csv("Data/Npy/01_discharge_training.csv")
disch_te = pd.read_csv("Data/Npy/01_discharge_test.csv")

prec_tr = np.load("Data/Npy/01_prec_training.npy")
prec_te = np.load("Data/Npy/01_prec_test.npy")

temp_tr = np.load("Data/Npy/01_temp_training.npy")
temp_te = np.load("Data/Npy/01_temp_test.npy")

# check dimensions
disch_tr.shape
disch_te.shape
disch_tr.head
disch_te.head

### Testplots of Prec + temp
plot_frame(temp_tr, prec_tr,mask, df, frame=11)
plot_frame(temp_te, prec_te,mask, df, frame=11)


### Plot test timeseries
def plot_streamflow(df, n_labels=10):
    fig, ax = plt.subplots()
    ax.plot(df['Time'], df['Q'])
    ax.xaxis.set_major_locator(plt.MaxNLocator(n_labels)) # limit number of dates on x axis
    fig.autofmt_xdate() # rotate data labels
    plt.show()

df.Time = pd.to_datetime(df.Time)
plotseq = pd.date_range(start=chopdate, end='2016-12-31', freq='D')
plot_streamflow(df[df.Time.isin(plotseq)],n_labels=20)

###  Image plotting function
def plot_frame(temp:np.array, prec:np.array, mask:np.array, df:pd.DataFrame, frame:int, limits:tuple=None):
    import matplotlib.pyplot as plt
    fig, ax = plt.subplots(1,2,figsize=(10,5))
    im_t = np.ma.masked_array(temp[frame], mask=mask)
    im_p = np.ma.masked_array(prec[frame], mask=mask)

    ax[0].set_title('Temperature')
    if limits is None:
        cb1 = ax[0].imshow(im_t)
    else:
        cb1 = ax[0].imshow(im_t, vmin=limits[0], vmax=limits[1])
    fig.colorbar(cb1, ax=ax[0], orientation='horizontal')

    ax[1].set_title('Precipitation')
    if limits is None:
        cb2 = ax[1].imshow(im_p)
    else:
        cb2 = ax[1].imshow(im_p, vmin=limits[2], vmax=limits[3])
    fig.colorbar(cb2, ax=ax[1], orientation='horizontal')

    for a in ax: a.axis('off')

    plt.show()
