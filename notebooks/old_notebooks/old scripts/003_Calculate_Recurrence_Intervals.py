import numpy as np
import pandas as pd
import os, sys
import matplotlib.pyplot as plt
import scipy.stats as stats
### import deephydro module
%load_ext autoreload
%autoreload 2
#module_folder = os.path.abspath('/src/')
module_folder = 'src/'
if module_folder not in sys.path:
   sys.path.append(module_folder)
from deephydro import data, visualization

data_folder = 'Data/Npy'
data_disc = '01_discharge.csv'

# load data
df = data.load_csv_data(data_folder=data_folder, filename=data_disc)
df_orig = data.load_csv_data(data_folder=data_folder, filename=data_disc) #for later
# add a year column
df = df.set_index(pd.DatetimeIndex(df.Time))
df["Year"] = df.index.year
df = df.loc[:,["Q","Year"]]
# Calculate annual max by resampling
df_max = df.resample('AS').max()
# Sort data smallest to largest
df_sorted = df.sort_values(by="Q")
# Count total obervations
n = df.shape[0]
# Add a numbered column 1 -> n to use in return calculation for rank
df_sorted.insert(0, 'rank', range(1, 1 + n))
# Calculate probability
df_sorted["probability"] = ((n - df_sorted["rank"] + 1) / (n + 1))
df_sorted["return-years"] = (1 / df_sorted["probability"])
# daily values
df_sorted["return-years"] = df_sorted["return-years"] / 365
df_sorted["probability"] = df_sorted["probability"] * 365

fig, ax = plt.subplots(figsize=(11, 6))
df_sorted.plot.scatter(y="Q",
                           x="return-years",
                           title="Return Period (Years)",
                           ax=ax,
                           color='grey',
                           fontsize=16,
                           logy=False,
                           label="Daily Mean Calculated")
ax.legend(frameon=True,
          framealpha=1)
ax.ticklabel_format(style="plain",axis="y")
ax.set_ylabel("Discharge")
ax.set_xlabel("Return Period (Years)")
ax.set_title(
    "Return Period (Years)")

plt.show()


# fit spline
from scipy.interpolate import CubicSpline
plt.scatter(df_sorted["return-years"], df_sorted.Q, label='Cubic Spline')
cubs = CubicSpline(df_sorted["return-years"], df_sorted.Q)
x_range = np.arange(np.min(df_sorted["return-years"]),max(df_sorted["return-years"]),step=0.1)
plt.plot(x_range, cubs(x_range), label='Cubic Spline')
plt.ylim(ymax = 4500, ymin = 0)

# Get discharge value of a ten-year flood
floodthresh = cubs(5)
# n above this
sum(df.Q>floodthresh)
flood_index = np.concatenate(np.where(df.Q>=floodthresh))
flooddates = df_orig.Time.iloc[flood_index]
np.array(flooddates)
##### NOT USED
### fit polynomial regression
plt.scatter(df_sorted["return-years"], df_sorted.Q, label='Cubic Spline')
coeffs = np.polyfit(df_sorted["return-years"], df_sorted.Q, 3)
x_range = np.arange(np.min(df_sorted["return-years"]),max(df_sorted["return-years"]),step=0.1)
predicted = np.polyval(coeffs,x_range)
plt.plot(x_range, predicted,)
#ax.set_ylim([0,4500])





