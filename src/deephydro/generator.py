from keras import utils
import numpy as np
import pandas

class ImageSequenceGenerator(utils.Sequence):
    '''Generates image sequences
    
    Supports:
    - uncertainty
    Does not support:
    - forecasting
    - day of year
    '''
    def __init__(self, data:np.array, labels:np.array, batch_size:int, 
                 n_frames:int, stride:int=1, 
                 doy:np.array=None, day_of_year:bool=True, 
                 uncertainty:bool=False, randomseed=None):
        '''
        Args:
            data:np.array
                Data set to sample from (x_train or x_test), shape=(N, H, W, C)
            labels:np.array
                Labels to sample from (y_train or y_test), shape=(N,)
            batch_size:int
                Batch size
            n_frames:int
                Number of consecutive frames in sample. Note: if `forecasting=True`, this also is the length of forecast
            stride:int
                step size between frames (default=1)
            doy:np.array,
                Data set of fractional day of year, shape=(N,)
            day_of_year:bool,
                Use fractional day of year. 
            uncertainty:bool
                Whether generator is used in uncertainty model. If `true`, adds dimension to labels to have same dimensionality as predictions.
            randomseed: int or None
                If not None, sets random seed
        '''
        self.data = data
        self.labels = labels
        self.batch_size = batch_size
        self.n_frames = n_frames
        self.stride = stride
        self.uncertainty = uncertainty
        self.doy = doy
        self.day_of_year = day_of_year
        self.randomseed = randomseed
        
        if self.randomseed is not None:
            np.random.seed(self.randomseed)

        assert len(self.data) == len(self.labels)
        if self.day_of_year:
            assert len(self.doy) == len(self.data)
            

    def __len__(self):
        '''Denotes the number of batches per epoch'''
        return int(np.floor(len(self.data) / self.batch_size))

    def __getitem__(self, index):
        '''Generate one batch of data'''
        
        batch_x, batch_y = [], []
        if self.day_of_year: batch_x_doy = []
            
        for _ in range(self.batch_size):
            # Get start index of sequence
            start_index = np.random.randint(0, len(self.data)-self.n_frames*self.stride)
            stop_index  = start_index + self.n_frames*self.stride

            # x = image sequence
            x = self.data[start_index:stop_index:self.stride]
            # y = streamflow at end point
            y = self.labels[stop_index-1] # fix for NOWcasting
            batch_x.append(x)
            batch_y.append(y)
            
            if self.day_of_year: 
                batch_x_doy.append( self.doy[stop_index])

        batch_x = np.array(batch_x)
        batch_y = np.array(batch_y)

        if self.uncertainty:
            # in case we are training a model with uncertainty
            # add an axis to make the targets y_true the same dimensionality as the predictions y_pred
            batch_y = batch_y[..., np.newaxis] # add axis
        
        if self.day_of_year:
            batch_x_doy = np.array(batch_x_doy)
            return [batch_x, batch_x_doy], batch_y
        else:
            return batch_x, batch_y


class ImageSequenceGeneratorBROKEN(utils.Sequence):
    '''Generates image sequences: '''
    def __init__(self,
                data:np.array,
                labels:np.array,
                batch_size:int,
                n_frames:int,
                doy:np.array=None,
                day_of_year=False,
                forecasting=False,
                uncertainty=False,
                randomseed=None):
        '''
        Args:
            data:np.array
                Data set to sample from (x_train or x_test), shape=(N, H, W, C)
            labels:np.array
                Labels to sample from (y_train or y_test), shape=(N)
            batch_size:int
                Batch size
            n_frames:int
                Number of consecutive frames in sample. Note: if `forecasting=True`, this also is the length of forecast
            doy:np.array
                Data containing fractional day of year, shape=(N)
            day_of_year:bool
                Whether to use fractional day of year as additional input. Note: incompatiable with `doy=None`.
            forecasting:bool
                Whether generator is used in forecasting model. If `true`, `n_frames` labels are generated instead of 1.
            uncertainty:bool
                Whether generator is used in uncertainty model. If `true`, adds dimension to labels to have same dimensionality as predictions.
            randomseed: int or None
                If not None, sets random seed
        '''
        self.data = data
        self.labels = labels
        self.doy = doy
        self.day_of_year = day_of_year
        self.batch_size = batch_size
        self.n_frames = n_frames
        self.forecasting = forecasting
        self.uncertainty = uncertainty
        self.randomseed = randomseed
        if self.randomseed is not None:
            np.random.seed(self.randomseed)
            
        assert len(self.data) == len(self.labels)
        if self.day_of_year:
            assert len(self.doy) == len(self.data)

        raise ValueError('This implementation seems to be broken!')

    def __len__(self):
        '''Denotes the number of batches per epoch'''
        return int(np.floor(len(self.data) / self.batch_size))

    def __getitem__(self, index):
        '''Generate one batch of data'''

        batch_x, batch_x_doy, batch_y = [], [], []

        for _ in range(self.batch_size):
            # Get start index of sequence
            # note, if forecasting, we should have gt data to forecast
            if self.forecasting:
                max_frame = len(self.data) - self.n_frames * 2
            else:
                max_frame = len(self.data) - self.n_frames
            start_index = np.random.randint(0, max_frame)
            stop_index  = start_index + self.n_frames

            # x = image sequence
            x = self.data[start_index:stop_index]

            if self.day_of_year:
                # day of year
                x_doy = self.doy[stop_index]
                batch_x_doy.append(x_doy)

            if self.forecasting:
                # y: streamflow from end point to n_frames ahead
                start = stop_index
                end   = start+self.n_frames
                y     = np.array(self.labels[start:end])
            else:
                # y: streamflow at end point
                y = self.labels[stop_index]

            batch_x.append(x)
            batch_y.append(y)

        batch_x = np.array(batch_x)
        batch_x_doy = np.array(batch_x_doy)
        batch_y = np.array(batch_y)

        if self.uncertainty:
            # in case we are training a model with uncertainty
            # add an axis to make the targets y_true the same dimensionality as the predictions y_pred
            batch_y = batch_y[..., np.newaxis] # add axis

        if self.day_of_year:
            return [batch_x, batch_x_doy], batch_y
        else:
            return batch_x, batch_y
