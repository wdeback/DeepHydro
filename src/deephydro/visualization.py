import matplotlib.pyplot as plt
import keras
import numpy as np
import pandas as pd

# --- plot streamflow ---

def plot_streamflow(df:pd.DataFrame, n_labels:int=10, figsize:tuple=(16,4), filename=None):
    '''Plot streamflow from dataframe.
    
    Streamflow (ground truth) assumed to be in df['Q'].
    If df['Q_pred'] exists, also plots predicted streamflow.
    If df['Q_pred_sigma'] exists, also plots predictive uncertainty as filled area.
    
    Args:
        df:pd.DataFrame
            dataframe with streamflow data in column 'Q' and date in 'Time'
        n_labels:int
            number of x labels    
        figsize:tuple
            size of figure    
        filename:str or None
            if not None, save figure to file
    '''
    fig, ax = plt.subplots(1, 1, figsize=figsize)
    ax.plot(df['Time'], df['Q'], label='measured')
    
    if 'Q_pred' in df.keys():
        ax.plot(df['Time'], df['Q_pred'], label='predicted')
        
        if 'Q_pred_sigma' in df.keys():
            def plot_uncertainty(column, color, label):
                ax.fill_between(df['Time'], 
                                df['Q_pred']-df[column],
                                df['Q_pred']+df[column],
                                color=color, alpha=0.2, 
                                label=label)
                
            plot_uncertainty('Q_pred_sigma', '#539caf', 'uncertainty')
            plot_uncertainty('Q_pred_sigma_epi', 'orange', 'epistemic')
             #plot_uncertainty('Q_pred_sigma_alea', 'blue', 'aleatoric')

        plt.legend()
        
    ax.xaxis.set_major_locator(plt.MaxNLocator(n_labels)) # limit number of dates on x axis
    ax.set_xlabel('Date')
    ax.set_ylabel('Streamflow')
    fig.autofmt_xdate() # rotate data labels
    plt.show()
    if filename is not None:
        fig.savefig(filename)

# --- plot streamflow ---

def plot_temp_prec_frame(temp:np.array, prec:np.array, mask:np.array, df:pd.DataFrame, frame:int, limits:tuple=None):
    '''Plot the temperature and precipitation at a particular frame
    
    Args:
        temp:np.array
            array with temperature data (N, H, W)
        prec:np.array
            array with precipitation data (N, H, W)
        mask:np.array
            array with catchment area mask (H, W)
        df:pd.DataFrame
            dataframe with 'Time' and streamflow 'Q' data (len=N)
        frame:int
            frame index to plot
        limits:tuple
            value limits (vmin, vmax)    
    '''
    import matplotlib.pyplot as plt
    fig, ax = plt.subplots(1,2,figsize=(10,5))
    im_t = np.ma.masked_array(temp[frame], mask=mask)
    im_p = np.ma.masked_array(prec[frame], mask=mask)

    ax[0].set_title('Temperature')
    if limits is None:
        cb1 = ax[0].imshow(im_t)
    else:
        cb1 = ax[0].imshow(im_t, vmin=limits[0], vmax=limits[1])
    fig.colorbar(cb1, ax=ax[0], orientation='horizontal')

    ax[1].set_title('Precipitation')
    if limits is None:
        cb2 = ax[1].imshow(im_p)
    else:
        cb2 = ax[1].imshow(im_p, vmin=limits[2], vmax=limits[3])
    fig.colorbar(cb2, ax=ax[1], orientation='horizontal')

    fig.suptitle("Date: " + df['Time'].iloc[frame], fontsize=14)
    for a in ax: a.axis('off')

    plt.show()

# ---- plot history ----

def _split_train_test_keys(history):
    '''
    Returns:
        tuple with e.g. (['loss', 'accuracy'], ['val_loss', 'val_accuracy'])
    '''
    # get loss and metrics keys
    keys = list(history.history.keys())
    # split training and test metrics
    keys_train = [key for key in keys if not key.startswith('val_')]
    keys_test  = list(set(keys) - set(keys_train))
    # remove learning rate, if present
    if 'lr' in keys_train:
        keys_train.remove('lr')
    # sort keys alphabetically
    keys_train, keys_test = sorted(keys_train), sorted(keys_test)

    # move 'loss' to front: https://stackoverflow.com/a/33933500
    loss_ind = keys_train.index('loss')
    keys_train.insert(0, keys_train.pop(loss_ind))
    # move 'val_loss' to front: https://stackoverflow.com/a/33933500
    loss_ind = keys_test.index('val_loss')
    keys_test.insert(0, keys_test.pop(loss_ind))

    return keys_train, keys_test

def plot_history(history:keras.callbacks.History, return_figure=False):
    '''Plot losses and metrics as a function of training time (epochs).

    Args:
        history: keras.callbacks.History
            history of model training
    Returns:
        matplotlib figure
    '''

    keys_train, keys_test = _split_train_test_keys(history)

    nr = len(keys_train)
    fig, ax = plt.subplots(nr, 1, figsize=(6,3*nr), sharex=True)
    fig.tight_layout(h_pad=3)
    ax = ax.flatten()

    for i, (key_train, key_test) in enumerate(zip(keys_train, keys_test)):
        ax[i].set_title(key_train)
        ax[i].plot(history.history[key_train], label=key_train)
        ax[i].plot(history.history[key_test], label=key_test)
        ax[i].legend()
        ax[i].grid()
    ax[i].set_xlabel('epoch')

    if return_figure:
        return fig

# ---- plot prediction ----

#def plot_prediction():

# ---- plot scatter plot ----

def plot_correlation(y_true:np.array, y_pred:np.array, y_pred_sigma:np.array=None, return_figure:bool=False, filename:str=None):
    '''
    
    Args:
    
    Returns:
    
    '''

    y_true, y_pred = np.squeeze(y_true), np.squeeze(y_pred)
    print(y_true.shape)
    print(y_pred.shape)
    with_uncertainty = False
    if y_pred.ndim > y_true.ndim:
        with_uncertainty = True
        y_pred_mean = y_pred[:,0]
        y_pred_std  = y_pred[:,1]
    else:
        y_pred_mean = y_pred

    from .metrics import ccc_numpy
    ccc = ccc_numpy(y_true, y_pred_mean)

    fig, ax = plt.subplots(1,1,figsize=(5,5))
    if y_pred_sigma is not None:
        ax.scatter(y_true, y_pred, c=y_pred_sigma, alpha=0.5)
    else:
        ax.scatter(y_true, y_pred, alpha=0.5)
    ax.axis('equal')
    ax.set_xlim(left=0)
    ax.set_ylim(bottom=0)
    ax.set_xlabel('True streamflow', fontsize=10)
    ax.set_ylabel('Predicted streamflow', fontsize=10)
    ax.plot(ax.get_xlim(), ax.get_ylim(), ls="--", c=".7")
    title = f'Concordance correlation coefficient = {ccc:.3f}'
    ax.set_title(title)
    plt.show()

    if filename is not None:
        fig.savefig(filename)
    if return_figure:
        return fig


# --- saliency map ----


def plot_saliency_map(image, shap_values, mask):

    from tqdm import tqdm_notebook as tqdm

    n_frames = len(image)
    fig, ax = plt.subplots(n_frames, 4, figsize=(2*6, n_frames*3))

    vmin_sal = np.min( shap_values )
    vmax_sal = np.max( shap_values )
    vmin_t = np.min( image[..., 0] )
    vmax_t = np.max( image[..., 0] )
    vmin_p = np.min( image[..., 1] )
    vmax_p = np.max( image[..., 1] )
    for frame, (im, sal)  in tqdm(enumerate(zip(image, shap_values)), total=len(image)):

        ax[frame,0].set_title(f'Saliency temp day {frame}')
        vmin = np.min( sal )
        vmax = np.max( sal )
        im_t = np.ma.masked_array(sal[..., 0], mask=mask)
        ax[frame,0].imshow(im_t, vmin=vmin_sal, vmax=vmax_sal)
        ax[frame,0].axis('off')

        ax[frame,1].set_title(f'Temp day {frame}')
        vmin = np.min( im )
        vmax = np.max( im )
        im_t = np.ma.masked_array(im[..., 0], mask=mask)
        ax[frame,1].imshow(im_t, vmin=vmin_t, vmax=vmax_t)
        ax[frame,1].axis('off')

        ax[frame,2].set_title(f'Saliency prec day {frame}')
        vmin = np.min( sal )
        vmax = np.max( sal )
        im_t = np.ma.masked_array(sal[..., 1], mask=mask)
        ax[frame,2].imshow(im_t, vmin=vmin_sal, vmax=vmax_sal)
        ax[frame,2].axis('off')

        ax[frame,3].set_title(f'Prec day {frame}')
        vmin = np.min( im )
        vmax = np.max( im )
        im_t = np.ma.masked_array(im[..., 1], mask=mask)
        ax[frame,3].imshow(im_t, vmin=vmin_p, vmax=vmax_p)
        ax[frame,3].axis('off')

    plt.show()

