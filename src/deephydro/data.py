import numpy as np
import os.path as osp
import warnings
import pandas as pd
import platform

# --- data loading ---

def load_csv_data(data_folder:str, filename:str):
    '''Load streamflow data from CSV (.csv).

    Args:
        data_folder:str
            path to filename
        filename:str
            filename (.csv)
    Returns:
        pandas DataFrame:
            df['Q']: streamflow
            df['DOY']: Day of year (0 - 366)
            df['FDOY']: Fractional day of year (0.0 - 1.0)
    '''
    # read csv file
    if platform.system()=='Windows': # added to avoid osp.join bug on windows
        df = pd.read_csv(data_folder+'/'+ filename)
    else:
        df = pd.read_csv(osp.join(data_folder, filename))
    # day of year
    df['DOY'] = pd.DatetimeIndex(df['Time']).dayofyear
    # calculate fractional day of year
    df['FDOY'] = pd.DatetimeIndex(df['Time']).dayofyear / 366
    return df

# --- data loading ---

def load_from_numpy(data_folder:str, filename:str):
    '''Load data from numpy (.npy).

    Args:
        data_folder:str
            path to filename
        filename:str
            filename (.npy)
    Returns:
        numpy array
    '''
    if platform.system()=='Windows': # added to avoid osp.join bug on windows
        return np.load(data_folder+'/'+ filename)
    else:
        return np.load(osp.join(data_folder, filename))

def load_from_tiff(data_folder:str, filename:str):
    '''Load data from TIFF image file (.tif or .tiff).

    Args:
        data_folder:str
            path to filename
        filename:str
            filename (.tif ot .tiff)
    Returns:
        numpy array
    '''
    warnings.warn('This function is untested! Use with care and check code.')
    from skimage.external import tifffile
    if platform.system()=='Windows': # added to avoid osp.join bug on windows
        return tifffile.imread(data_folder+'/'+ filename)
    else:
        return tifffile.imread(osp.join(data_folder, filename))


def load_from_netcdf(data_folder:str, filename:str, varname:str):
    '''Load data from netCDF4 data.

    Args:
        data_folder:str
            path to filename
        filename:str
            filename (.nc)
        varname:str
            name of variable to extract
    Returns:
        numpy array
            with data in varname (T, H, W)
        mask
            data mask of unavailable data (H, W)
    '''
    warnings.warn('This function is untested! Use with care and check code.')
    try:
        import netCDF4
    except:
        raise ImportError('NetCDF4 not found, please `pip install netCDF4`')
    if platform.system()=='Windows': # added to avoid osp.join bug on windows
        dat = netCDF4.Dataset(data_folder+'/'+ filename)
    else:
        dat = netCDF4.Dataset(os.path.join(data_folder, filename))
    if varname not in dat.variables.keys():
        raise ValueError(f'{varname} not in {dat.variables.keys()}')
    arr = np.array(dat.variables[varname])
    mask = arr < -100
    arr[ mask ] = 0.0
    return arr, mask[0]



# --- train test split ---

def split_train_validation(x:np.array, y:np.array,
                            validation_fraction:float=0.2,
                            validation_first=False):
    '''Split training and validation data.

    Args:
        x:np.array
            input data (N,H,W,2)
        y:np.array
            targets (N)
        validation_fraction:float
            fraction of x to use for validation
        validation_first:bool
            use first part of x for validation. if False, use last part for validation.
    '''

    assert len(x) == len(y), f'Number of targets ({len(y)}) is not equal to number of data samples ({len(x)})'
    val_size = int(validation_fraction * len(x))
    train_size = len(x) - val_size

    if validation_first:
        x_train, x_val, y_train, y_val = x[val_size:], x[:val_size], y[val_size:], y[:val_size]
    else:
        x_train, x_val, y_train, y_val = x[:train_size], x[train_size:], y[:train_size], y[train_size:]

    return x_train, x_val, y_train, y_val

# --- train test split ---

def split_DOY_train_validation(x_doy:np.array,
                                validation_fraction:float=0.2,
                                validation_first=False):
    '''Split training and validation data.

    Args:
        x_doy:np.array
            input fraction day of year data (N,)
        validation_fraction:float
            fraction of x to use for validation
        validation_first:bool
            use first part of x for validation. if False, use last part for validation.
    '''

    val_size = int(validation_fraction * len(x_doy))
    train_size = len(x_doy) - val_size

    if validation_first:
        x_doy_train, x_doy_val = x_doy[val_size:], x_doy[:val_size]
    else:
        x_doy_train, x_doy_val = x_doy[:train_size], x_doy[train_size:]

    return x_doy_train, x_doy_val

def split_df_train_validation(df:pd.DataFrame,
                                validation_fraction:float=0.2,
                                validation_first=False):
    '''Split training and validation data.

    Args:
        df:pd.DataFrame
            dataframe of len(N)
        validation_fraction:float
            fraction of x to use for validation
        validation_first:bool
            use first part of x for validation. if False, use last part for validation.
    '''

    val_size = int(validation_fraction * len(df))
    train_size = len(df) - val_size

    if validation_first:
        df_train, df_val = df[val_size:], df[:val_size]
    else:
        df_train, df_val = df[:train_size], df[train_size:]

    return df_train, df_val

# --- standardization ---

def _standardize(data:np.array, logarithm:bool=False, return_moments=False):
    '''Standardize data: zero-centering and dividing by standard deviation.
    
    Args:
        data:np.array
            data to standardize
        logarithm:bool
            if True, logarithmize output (not mean and std) 
        return_moments:bool
            if True, mean and std are returned (useful for rescaling)

    Returns:
        np.array
            standardized data
        tuple
            if `return_moments=True`, mean and standard deviation
    '''

    if logarithm:
        data = np.log(data)

    mean = np.mean(data)
    std = np.std(data)
    standardized = (data-mean)/std
    
    if return_moments:
        return standardized, (mean, std)
    else:
        return standardized

def standardize_input_data(x:np.array):
    '''Standardize input data: zero-centering and dividing by standard deviation.
    
    Args:
        x:np.array
            input data to standardize, shape=(N, H, W, 2)

    Returns:
        np.array
            standardized data, shape=(N, H, W, 2)
    '''
    
    x_temp = x[..., 0]
    x_prec = x[..., 1]
    
    x_temp_standard = _standardize(x_temp)
    x_prec_standard = _standardize(x_prec)
    
    return np.stack([ x_temp_standard, x_prec_standard], axis=-1)

def standardize_targets(y:np.array):
    '''Standardize targets: zero-centering and dividing by standard deviation.
    
    Args:
        y:np.array
            targets to standardize, shape=(N,)

    Returns:
        np.array
            standardized data, shape=(N, H, W, 2)
        tuple
            mean and standard deviation
    '''
    
    return _standardize(y, logarithm=False, return_moments=True)
    