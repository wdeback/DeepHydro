from keras import losses
from keras import backend as K

# -- several regression losses --

def mae(y_true, y_pred):
    '''Mean absolute error (L1 loss)'''
    return losses.mae(y_true, y_pred)

def mse(y_true, y_pred):
    '''Mean squared error (L2 loss)'''
    return losses.mse(y_true, y_pred)

def logcosh(y_true, y_pred):
    '''Logarithm of the hyperbolic cosine'''
    return losses.logcosh(y_true, y_pred)

def huber(delta:float=1.0):
    '''Huber loss, aka smooth l1 loss, https://en.wikipedia.org/wiki/Huber_loss
    Args:
        delta: float
            value to switch between L2 (below) and L1 (above)
    Usage:
        `model.compile(loss=losses.huber(delta=1.0))`
    '''
    from tensorflow.losses import huber_loss
    def loss(y_true, y_pred):
        return huber_loss(y_true, y_pred, delta=delta)

# -- regression losses with estimated uncertainty --

def gauss(y_true, y_pred):
    '''Gaussian loss to estimate aleatoric uncertainty, after Kendall and Gal (2017).
    See eq. 8 in https://arxiv.org/pdf/1703.04977.pdf
    '''
    assert K.ndim(y_true) == K.ndim(y_pred), f'Number of dimensions for y_true ({K.ndim(y_true)}) and y_pred ({K.ndim(y_pred)}) must be equal'
    mu_gt  = y_true[...,0]
    mu     = y_pred[...,0]
    logvar = y_pred[...,1] + K.epsilon()
    return 0.5 * K.exp(-logvar)*(K.square(mu_gt - mu)) + 0.5*logvar


def laplace(y_true, y_pred):
    '''Laplace loss to estimate aleatoric uncertainty.
    Similar to eq. 8 in https://arxiv.org/pdf/1703.04977.pdf
    See CARE implementation: https://github.com/CSBDeep/CSBDeep/blob/7e8de81e90de7509fa289bd12eec68b12cc66550/csbdeep/internals/losses.py#L17
    '''
    assert K.ndim(y_true) == K.ndim(y_pred), f'Number of dimensions for y_true ({K.ndim(y_true)}) and y_pred ({K.ndim(y_pred)}) must be equal'
    mu_gt = y_true[...,0] # true mean
    mu    = y_pred[...,0] # predicted mean
    beta  = y_pred[...,1] + K.epsilon() # predicted scale parameter
    return K.abs(mu_gt - mu)/(beta) + K.log(beta) + np.log(2.0)
