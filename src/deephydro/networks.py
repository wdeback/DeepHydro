
# --- 3D convolutional model ---

def Conv3D_model(input_shape:tuple, kernel_depth:int=5, n_layers:int=3, n_filters:int=16):

    from keras import layers, models
    i = layers.Input(shape=input_shape) # (?, n_frames, height, width, channels)
    x = i
    
    for _, _ in enumerate(range(n_layers)):
        x = layers.Conv3D(n_filters,
                            kernel_size=(3,3,kernel_depth),
                            strides=(2,2,1),
                            activation='relu')(x)
        x = layers.BatchNormalization()(x)
        n_filters *= 2

    x = layers.Conv3D(n_filters,
                        kernel_size=(3,3,kernel_depth),
                        strides=(2,2,1),
                        activation='relu')(x)

    x = layers.Flatten()(x)
    x = layers.Dense(128, activation='relu')(x)
    o = layers.Dense(1, activation='linear')(x)
    return models.Model(inputs=[i], outputs=[o])


# --- feature extractor ---

def feature_extractor(input_shape:tuple, n_layers:int=3, n_filters:int=16, batch_norm:bool=False, dropout_rate:float=0.0, conv2D_preprocessing=True):
    '''
    Feature extraction ConvLSTM model, used by all models below.

    Args:
        input_shape: tuple
            shape of input tensor without batch size: (n_frames, height, width, channels)
        n_layers: int
            number of ConvLSTM layers
        n_filters: int
            number of filters in first ConvLSTM layer (multiplied by 2 for every subsequent downsampled layer)
        batch_norm: bool
            use batch normalization (can we do this with ConvLSTMs?)
        dropout_rate: float
            dropout rate
        conv2D_preprocessing: bool
            if True, add time-distributed conv2d layers for preprocessing

    '''
    
    init_n_filters = n_filters

    from keras import layers, models
    i = layers.Input(shape=input_shape, name='images') # (?, n_frames, height, width, channels)
    x = i

    # convolutional preprocessing layers
    if conv2D_preprocessing:
        for _ in range(1):
            x = layers.TimeDistributed( layers.Conv2D(n_filters, kernel_size=(3,3), strides=(1,1), padding='same', activation='relu'))(x)
            n_filters *= 2
    
        #n_filters = init_n_filters
        n_filters //= 2
    
    # convolutional LSTM layers    
    for _ in range(n_layers):
        # these ConvLSTM layers output sequence: `return_sequences=True`
        x = layers.ConvLSTM2D(n_filters, kernel_size=(3,3), strides=(2,2),
                              activation='tanh', recurrent_activation='hard_sigmoid',
                              return_sequences=True)(x)
        if dropout_rate > 0.:
            x = layers.Dropout(rate=dropout_rate)(x)
        if batch_norm:
            x = layers.BatchNormalization()(x)
        n_filters *= 2

    model = models.Model(inputs=[i], outputs=[x], name='feature extractor')
    return model, n_filters

# --- nowcasting without uncertainties ---

# def nowcasting_model_simple(input_shape:tuple, n_layers:int=3, n_filters:int=16):
#     '''ConvLSTM model to predict streamflow from image sequence (nowcasting)

#     - Does not have input for day of year
#     - Does not have dropout

#     Use with losses.mse, losses.logcosh or losses.huber'''

#     from keras import layers, models
#     fe, n_filters = feature_extractor(input_shape, n_layers, n_filters)
#     x = fe.output

#     # this ConvLSTM layer only returns value for last point in sequence: `return_sequence = False`
#     x = layers.ConvLSTM2D(n_filters, kernel_size=(3,3), strides=(2,2),
#                          activation='tanh', recurrent_activation='hard_sigmoid', return_sequences=False)(x)

#     x = layers.Flatten()(x)
#     x = layers.Dense(128, activation='relu')(x)
#     o = layers.Dense(1, activation='linear')(x)

#     return models.Model(inputs=[fe.input], outputs=[o])



def nowcasting_model(input_shape:tuple, n_layers:int=3, n_filters:int=16, 
                     dropout_rate:float=0.5, dayofyear:bool=False, 
                     batch_norm:bool=True, conv2D_preprocessing:bool=True):
    '''ConvLSTM model to predict streamflow from image sequence (nowcasting)

    Use with losses.mse, losses.logcosh or losses.huber'''

    from keras import layers, models
    fe, n_filters = feature_extractor(input_shape, n_layers, n_filters,
                                     dropout_rate=dropout_rate,
                                     batch_norm=batch_norm,
                                     conv2D_preprocessing=conv2D_preprocessing)
    x = fe.output

    # this ConvLSTM layer only returns value for last point in sequence: `return_sequence = False`
    x = layers.ConvLSTM2D(n_filters, kernel_size=(3,3), strides=(2,2),
                         activation='tanh', recurrent_activation='hard_sigmoid', return_sequences=False)(x)

    x = layers.Flatten()(x)

    if dayofyear:
        i = layers.Input((1,), name='day_of_year')
        doy = layers.Dense(32, activation='relu')(i)
        x = layers.Concatenate(axis=-1)([x, doy])

    units = int(256*(1.0/dropout_rate))
    print(units)
    x = layers.Dense(units if not dayofyear else units+32, activation='relu')(x)
    x = layers.Dropout(rate=dropout_rate)(x)
    o = layers.Dense(1, activation='linear')(x)

    if dayofyear:
        model = models.Model(inputs=[fe.input, i], outputs=[o])
    else:
        model = models.Model(inputs=[fe.input], outputs=[o])
    return model


# --- nowcasting with uncertainties ---

def nowcasting_uncertainty_model(input_shape:tuple, n_layers:int=3, n_filters:int=16, 
                                 dropout_rate:float=0.5, dayofyear:bool=False, 
                                 batch_norm:bool=True, conv2D_preprocessing:bool=True):
    '''ConvLSTM model to predict streamflow + uncertainties from image sequence

    Use with losses.gauss or losses.laplace
    '''
    from keras import layers, models
    fe, n_filters = feature_extractor(input_shape, n_layers, n_filters,
                                     dropout_rate=0.0,
                                     batch_norm=False,
                                     conv2D_preprocessing=conv2D_preprocessing)
    x = fe.output

    # this ConvLSTM layer only returns value for last point in sequence: `return_sequence = False`
    x = layers.ConvLSTM2D(n_filters, kernel_size=(3,3), strides=(2,2),
                         activation='tanh', recurrent_activation='hard_sigmoid', return_sequences=False)(x)

    x = layers.Flatten()(x)

    if dayofyear:
        i = layers.Input((1,), name='day_of_year')
        doy = layers.Dense(32, activation='relu')(i)
        x = layers.Concatenate(axis=-1)([x, doy])

    units = int(256*(1.0/dropout_rate))
    x = layers.Dense(units if not dayofyear else units+32, activation='relu')(x)
    x = layers.Dropout(rate=dropout_rate)(x, training=True) # train + inference-time Dropout for epistemic uncertainty
    
    x = layers.Dense(units//2, activation='relu')(x)
    x = layers.Dropout(rate=dropout_rate)(x, training=True) # train + inference-time Dropout for epistemic uncertainty

    mu     = layers.Dense(1, activation='linear')(x) # predicted mean
    logvar = layers.Dense(1, activation='linear')(x) # predicted log variance ('softplus' activation would give the variance, not logvar!)
    o      = layers.Concatenate(axis=-1)([mu, logvar])

    if dayofyear:
        model = models.Model(inputs=[fe.input, i], outputs=[o])
    else:
        model = models.Model(inputs=[fe.input], outputs=[o])
    return model
        
    
#     from keras import layers, models, initializers
#     fe, n_filters = feature_extractor(input_shape, n_layers, n_filters)
#     x = fe.output

#     # this ConvLSTM layer only returns value for last point in sequence: `return_sequence = False`
#     x = layers.ConvLSTM2D(n_filters, kernel_size=(3,3), strides=(2,2),
#                          activation='tanh', recurrent_activation='hard_sigmoid', return_sequences=False)(x)

#     x = layers.Flatten()(x)
#     x = layers.Dense(int(128*(1.0/dropout_rate)), activation='relu')(x)
#     x = layers.Dropout(rate=dropout_rate)(x, training=True) # train + inference-time Dropout for epistemic uncertainty
#     mu     = layers.Dense(1, activation='linear')(x) # predicted mean
#     logvar = layers.Dense(1, activation='linear')(x) # predicted log variance ('softplus' activation)
#     o      = layers.Concatenate(axis=-1)([mu, logvar])
#     return models.Model(inputs=[fe.input], outputs=[o])


# # --- forecasting without uncertainties ---

# def forecasting_model(input_shape:tuple, n_layers:int=3, n_filters:int=16):
#     '''ConvLSTM model for streamflow forecasting from image sequence'''

#     from keras import layers, models, backend
#     fe, n_filters = feature_extractor(input_shape, n_layers, n_filters)
#     x = fe.output

#     # this ConvLSTM layer only returns values for whole sequence: `return_sequence = True`
#     x = layers.ConvLSTM2D(n_filters, kernel_size=(3,3), strides=(2,2),
#                          activation='tanh', recurrent_activation='hard_sigmoid',
#                          return_sequences=True)(x)

#     x = layers.TimeDistributed(layers.Flatten())(x)
#     x = layers.TimeDistributed(layers.Dense(128, activation='relu'))(x)
#     o = layers.TimeDistributed(layers.Dense(1, activation='linear'))(x)
#     #o = layers.Lambda(lambda x: backend.squeeze(x, axis=-1))(o) # get rid of empty axis
#     return models.Model(inputs=[fe.input], outputs=[o])

# # --- forecasting with uncertainties ---

# def forecasting_uncertainty_model(input_shape:tuple, n_layers:int=3, n_filters:int=16, dropout_rate:float=0.5):
#     '''ConvLSTM model for streamflow forecasting from image sequence'''

#     from keras import layers, models
#     fe, n_filters = feature_extractor(input_shape, n_layers, n_filters)
#     x = fe.output

#     # this ConvLSTM layer only returns values for whole sequence: `return_sequence = True`
#     x = layers.ConvLSTM2D(n_filters, kernel_size=(3,3), strides=(2,2),
#                          activation='tanh', recurrent_activation='hard_sigmoid',
#                          return_sequences=True)(x)

#     x = layers.TimeDistributed(layers.Flatten())(x)
#     x = layers.TimeDistributed(layers.Dense(int(128*(1.0/dropout_rate)), activation='relu'))(x)
#     x = layers.TimeDistributed(layers.Dropout(rate=dropout_rate))(x, training=True) # train + inference-time Dropout for epistemic uncertainty
#     mu     = layers.TimeDistributed(layers.Dense(1, activation='linear'))(x) # predicted mean
#     logvar = layers.TimeDistributed(layers.Dense(1, activation='softplus'))(x) # predicted log variance
#     o      = layers.Concatenate()([mu, logvar])
#     return models.Model(inputs=[fe.input], outputs=[o])


# def forecasting_uncertainty_model2(input_shape:tuple, n_layers:int=3, n_filters:int=16, dropout_rate:float=0.5):
#     '''ConvLSTM model for streamflow forecasting from image sequence

#     Now with LSTMs instead of TimeDistributed(Dense)
#     '''

#     from keras import layers, models
#     fe, n_filters = feature_extractor(input_shape, n_layers, n_filters)
#     x = fe.output

#     # this ConvLSTM layer only returns values for whole sequence: `return_sequence = True`
#     x = layers.ConvLSTM2D(n_filters, kernel_size=(3,3), strides=(2,2),
#                          activation='tanh', recurrent_activation='hard_sigmoid',
#                          return_sequences=True)(x)

#     x = layers.TimeDistributed(layers.Flatten())(x)
#     x = layers.LSTM(int(128*(1.0/dropout_rate)), activation='tanh', recurrent_activation='hard_sigmoid', return_sequences=True)(x)
#     x = layers.Dropout(rate=dropout_rate)(x, training=True) # train + inference-time Dropout for epistemic uncertainty
#     mu     = layers.LSTM(1, activation='tanh', recurrent_activation='hard_sigmoid', return_sequences=True)(x) # predicted mean
#     logvar = layers.LSTM(1, activation='tanh', recurrent_activation='hard_sigmoid', return_sequences=True)(x) # predicted log variance
#     o      = layers.Concatenate()([mu, logvar])
#     return models.Model(inputs=[fe.input], outputs=[o])


# --- nowcasting with 3D convolutional model ---

def CNN_model(input_shape:tuple, n_layers:int=4, n_filters:int=32):
    '''CNN model: predict streamflow from single temp/prec image

    ONLY FOR EDUCATIONAL PURPOSES
    '''

    from keras import layers, models
    print(input_shape)
    i = layers.Input(shape=input_shape)
    x = i
    for _ in range(n_layers):
        x = layers.Conv2D(n_filters, kernel_size=(3,3), strides=(2,2))(x)
        x = layers.BatchNormalization()(x)
        x = layers.Activation('relu')(x)
        n_filters *= 2

    x = layers.GlobalAveragePooling2D()(x)
    x = layers.Dense(128, activation='relu')(x)
    o = layers.Dense(1, activation='linear')(x)

    return models.Model(inputs=[i], outputs=[o])