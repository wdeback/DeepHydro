
def ccc_numpy(y_true, y_pred):
    '''Lin's Concordance correlation coefficient in numpy
    https://en.wikipedia.org/wiki/Concordance_correlation_coefficient'''
    import numpy as np

#     print(f'y_pred.shape = {y_pred.shape}')
#     print(f'y_true.shape = {y_true.shape}')
#     print(f'y_pred.ndim = {y_pred.ndim}')
#     print(f'y_true.ndim = {y_true.ndim}')
    y_true = np.squeeze(y_true)
    y_pred = np.squeeze(y_pred)
    if y_pred.ndim > y_true.ndim:
        print('...')
        # in case y_pred contains uncertainty axis,
        # only use the mean prediction
        y_pred = y_pred[...,0]

    # covariance between y_true and y_pred
#     print(f'y_pred.shape = {y_pred.shape}')
#     print(f'y_true.shape = {y_true.shape}')
#     print(f'y_pred.ndim = {y_pred.ndim}')
#     print(f'y_true.ndim = {y_true.ndim}')

    def ccc(x,y):
        print(x.shape)
        print(y.shape)
        s_xy = np.cov([x, y], bias=True)[0,1] #[1,0] is the identical covariance between y_pred and y_true

        # means
        x_m = np.mean(x)
        y_m = np.mean(y)
        # variances
        s_x_sq = np.var(x)
        s_y_sq = np.var(y)

        # condordance correlation coefficient
        ccc = (2.0*s_xy) / (s_x_sq + s_y_sq + (x_m-y_m)**2)
        return ccc

    return ccc(y_true,y_pred)

def ccc(y_true, y_pred):
    '''Lin's Concordance correlation coefficient in keras
    https://en.wikipedia.org/wiki/Concordance_correlation_coefficient'''

    from keras import backend as K

    # print("Before:")
    # print("y_true.shape = ", K.int_shape(y_true))
    # print("y_pred.shape = ", K.int_shape(y_pred))

    if K.int_shape(y_pred)[-1] is not None:
        # in case y_pred contains uncertainty axis,
        # only use the mean prediction
        y_pred = y_pred[...,0]
        y_true = y_true[...,0]
        # print('Taking only the means...')

    # print("After:")
    # print("y_true.shape = ", K.int_shape(y_true))
    # print("y_pred.shape = ", K.int_shape(y_pred))

    # means
    x_m = K.mean(y_true)
    y_m = K.mean(y_pred)
    # variances
    s_x_sq = K.var(y_true)
    s_y_sq = K.var(y_pred)

    # covariance between y_true and y_pred
    s_xy = K.mean(y_true*y_pred, axis=-1) - x_m*y_m
    #print('cov keras: ', K.eval(s_xy))

    # condordance correlation coefficient
    return (2.0*s_xy) / (s_x_sq + s_y_sq + (x_m-y_m)**2)

def test_ccc():
    '''Test whether ccc_numpy() and ccc_keras() return the same result'''
    import numpy as np
    from keras import backend as K

    a = np.random.random((100,))
    b = np.random.random((100,2))

    result_numpy = ccc_numpy(a,b)
    print(result_numpy)
    result_keras = K.eval(ccc_keras(K.variable(a), K.variable(b)))
    print(result_keras)
    assert np.allclose(result_numpy, result_keras, rtol=1e-5), 'ccc_numpy returned different result as ccc_keras.'
