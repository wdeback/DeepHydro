# ---- prediction ----

def predict(model:keras.models.Model, 
            image:np.array, 
            y_mean:float=None, 
            y_std:float=None, 
            gt:np.array=None, 
            plot:bool=False):
    '''
    Predict streamflow from input data.

    Args:
        model: keras.models.Model
            train keras model
        image: np.array
            input image, shape = (H, W, C)
        y_mean: float
            mean y calculated from training set, for rescaling
        y_std: float
            mean y calculated from training set, for rescaling
        gt: np.array
            ground truth data (for plotting)
        plot: bool
            create figure
    '''
    
    # add batch dimension
    im = image[np.newaxis, ...] 
    # get prediction
    y_pred = model.predict(im)

    # remove empty axes, e.g. batch axis
    y_pred = np.squeeze(y_pred)

    with_uncertainty = False
    if y_pred.ndim == 2:
        with_uncertainty = True
    
    if with_uncertainty:
        y_pred_mean = y_pred[0] # mean
        y_pred_std  = np.sqrt(np.exp(y_pred[1])) # logvar -> std
    else:
        y_pred_mean = y_pred

    # rescale prediction 
    with_rescaling = False
    if y_mean is not None and y_std is not None:
        with_rescaling = True

    if with_rescaling:
        y_pred_mean = y_pred_mean * y_std + y_mean
        if with_uncertainty:
            y_pred_std = y_pred_std * y_std
    else:
        print('Warning: values not rescaled!')

    if with_uncertainty:
        return y_pred_mean, y_pred_std
    else:
        return y_pred_mean

def predict_batch(model:keras.models.Model, 
                    generator,
                    n_samples:int=None,
                    #chunk_size:int=128,
                    y_mean:float=None, 
                    y_std:float=None, 
                    gt:np.array=None, 
                    plot:bool=False):
        '''Predict a batch of samples
        
        Args:
            model:
                trained model
            generator:
                batch generator
            n_samples:
                number of samples to predict
            y_mean: float
                mean y calculated from training set, for rescaling
            y_std: float
                mean y calculated from training set, for rescaling
            gt: np.array
                ground truth data (for plotting)
            plot: bool
                create figure
        Returns:
            ccc
        '''

        predictions  = np.array([])
        ground_truth = np.array([])
        while len(predictions) < n_samples:
            print(f'{len(predictions)} / {n_samples}')
            
            batch = generator.__getitem__(0)
            y_preds = np.squeeze(model.predict(batch[0]))
            y_trues = np.squeeze(batch[1])
            
            # rescale prediction 
            with_rescaling = False
            if y_mean is not None and y_std is not None:
                with_rescaling = True

            if with_rescaling:
                y_preds = y_preds * y_std + y_mean
                y_trues = y_trues * y_std + y_mean
                
            predictions  = np.concatenate([predictions, y_preds]) #if predictions.size else y_preds
            ground_truth = np.concatenate([ground_truth, y_trues]) #if ground_truth.size else y_trues
            
        return predictions[:n_samples], ground_truth[:n_samples]

def predict_uncertainty(model, data, T:int=10):
    '''
    Predict the mean and uncertainty (standard deviation) 
    by multiple `T` forward passes, following Kendall and Gal (2007).
    
    Args:
        model:
            trained model with inference-time dropout
        data:array-like
            input data
        T:int
            number of forward passes
    
    Returns:
        mu:float
            predicted mean value (not rescaled)
        variances:tuple
            variance_p:float
                predictive uncertainty, sum of epistemic and aleatoric, variance (sigma^2) (not rescaled)
            variance_e:float
                epistemic uncertainty, model uncertainty, variance (sigma^2) (not rescaled)
            variance_a:float
                aleatoric uncertainty, data-inherent uncertainty, variance (sigma^2) (not rescaled)
    '''

#     if n_gpus > 1:
#         from keras.utils import multi_gpu_model
#         parallel_model = multi_gpu_model(model, gpus=n_gpus)
#         output = np.array([parallel_model.predict(data) for _ in range(T)])
#     else:
#         output = np.array([model.predict(data) for _ in range(T)])
    
    # perform T forward passes
    try: # use parallel processing if joblib available
        from joblib import Parallel, delayed
        output = np.array(Parallel(n_jobs=4)(delayed(model.predict(data))for _ in range(T)))
    except ImportError:
        output = np.array([model.predict(data) for _ in range(T)])
        
    y_hats, logvars = output[..., 0].T, output[..., 1].T
    
    # from logvariances to variances
    sigmasqs = np.exp(logvars)
    
    # predicted variance, combining aleatoric and epistemic uncertainties
    # eq. 8 of Kendall and Gal (2017)
    def predictive_uncertainty(y_hat, sigmasq):
        return np.mean(np.square(y_hat)) - np.square(np.mean(y_hat)) + np.mean(sigmasq) 
    
    variances_p = np.array([ predictive_uncertainty(y_hat, sigmasq) for y_hat, sigmasq in zip(y_hats, sigmasqs)])

    # aleatoric uncertainty
    variances_a = np.mean(sigmasqs, axis=-1)
    
    # epistemic uncertainty
    variances_e = np.array([np.mean(np.square(y_hat)) - np.square(np.mean(y_hat)) for y_hat in y_hats])

    # predicted means and stds
    mus  = np.mean(y_hats, axis=-1)

    return mus, (variances_p, variances_e, variances_a)
    
    
def predict_timeseries(model:keras.models.Model, 
                        data:np.array,
                        doy:np.array,
                        df:pd.DataFrame,
                        n_frames:int,
                        stride:int=2,
                        batch_size:int=128,
                        Y_MEAN:float=None, 
                        Y_STD:float=None, 
                        gt:np.array=None, 
                        plot:bool=False,
                        uncertainty:int=False,
                        T:int=10,
                        n_gpus:int=4):
        '''Predict a batch of samples
        
        Args:
            model:
                trained model
            data:
                image data, shape (N,H,W,2)
            doy:np.array or None
                day of year, shape = (N,) 
            df: pd.DataFrame
                dataframe of length N with df['Time']
            n_frames: int
                number of frames for prediction
            stride: int
                stride for prediction
            batch_size:int
                number of inputs to process
            Y_MEAN: float
                mean y calculated from training set, for rescaling
            Y_STD: float
                mean y calculated from training set, for rescaling
            gt: np.array
                ground truth data (for plotting)
            plot: bool
                create figure
            uncertainty: bool
                if True, assumes uncertainty is returned by model
            T: int
                if uncertainty=True, perform T forward passes (only relevant in model with inference-time dropout)
            n_gpus: int
                number of GPUs to use

        Returns:
            predictions: np.array
                rescaled predictions time series
            ground_truth: np.array
                ground truth time series
            df: pd.DataFrame
                dataframe with predictions, ground truth and uncertainties (if available)
        '''
        
        if n_gpus > 1:
            from keras.utils import multi_gpu_model
            model = multi_gpu_model(model, gpus=n_gpus)
            print(f'Running inference on {n_gpus} GPUs.')

        predictions  = np.array([])
        if uncertainty: 
            uncertainties = np.array([])
            uncertainties_epi = np.array([])
            uncertainties_alea = np.array([])
        ground_truth = np.array([])
        n_samples = (len(data)-n_frames) // stride
        index = 0
        
        while len(predictions) < n_samples:
            print(f'{len(predictions)} / {n_samples}')
            
            # get a batch of inputs (largely overlapping)
            iterator = range(index, min(index+batch_size, n_samples), stride)
            batch = np.array([data[i : i+n_frames] for i in iterator])
            if doy is not None:
                batch_doy = np.array([doy[i+n_frames] for i in iterator])
            y_trues = np.array([gt[i+n_frames] for i in iterator])
            
            index += batch_size
            
            if doy is None:
                input_data = batch
            else:
                input_data = [batch, batch_doy]
                
            if uncertainty:
                y_preds, y_vars_all = predict_uncertainty(model, input_data, T=T)
                y_vars, y_vars_epi, y_vars_alea = y_vars_all
            else:
                y_preds = np.squeeze(model.predict(input_data))
            
            # rescale prediction 
            with_rescaling = False
            if Y_MEAN is not None and Y_STD is not None:
                with_rescaling = True

            if with_rescaling:
                y_preds = y_preds * Y_STD + Y_MEAN
                y_trues = y_trues * Y_STD + Y_MEAN
                if uncertainty: 
                    y_stds      = np.sqrt(y_vars) * Y_STD
                    y_stds_epi  = np.sqrt(y_vars_epi) * Y_STD
                    y_stds_alea = np.sqrt(y_vars_alea) * Y_STD

#             if with_rescaling:
#                 y_preds = np.exp(y_preds * Y_STD + Y_MEAN)
#                 y_trues = np.exp(y_trues * Y_STD + Y_MEAN)
#                 if uncertainty: 
#                     y_stds = np.exp(y_stds * Y_STD) 
                
#             if with_rescaling:                
#                 y_trues = np.exp(y_trues*Y_STD + Y_MEAN)
#                 y_preds_log = y_preds*Y_STD + Y_MEAN
                
#                 if uncertainty: 
#                     y_vars_log = y_vars*Y_STD                     
#                     y_preds = np.exp(y_preds_log + y_vars_log/2)
#                     y_vars  = (np.exp(y_vars_log)-1) * np.exp(2*y_preds+y_vars_log)
                    
                
            predictions  = np.concatenate([predictions, y_preds])
            ground_truth = np.concatenate([ground_truth, y_trues])
            if uncertainty: 
                uncertainties      = np.concatenate([uncertainties, y_stds])
                uncertainties_epi  = np.concatenate([uncertainties_epi, y_stds_epi])
                uncertainties_alea = np.concatenate([uncertainties_alea, y_stds_alea])
            
            
        import pandas as pd            
        df = pd.DataFrame({'Time':df['Time'][:-n_frames],
                            'Q':ground_truth,
                            'Q_pred':predictions})
        if uncertainty:
            df['Q_pred_sigma'] = uncertainties
            df['Q_pred_sigma_epi'] = uncertainties_epi
            df['Q_pred_sigma_alea'] = uncertainties_alea
            
        if plot:
            from .visualization import plot_streamflow
            plot_streamflow(df, n_labels=12)
                
        return predictions, ground_truth, df
            
