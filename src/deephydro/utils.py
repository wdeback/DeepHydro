import keras
import numpy as np
import pandas as pd

# ---- multi gpu support ----

def multi_gpu(model:keras.models.Model, num_gpu:int=4):
    '''
    Create parallel model for training on multiple GPUs

    See: https://keras.io/utils/
    
    Args:
        model: keras.models.Model
            model to parallelize
        num_gpu: int
            number of GPUs available
    Returns:
        parallel model
    '''
    from keras import utils
    return utils.multi_gpu_model(model, gpus=num_gpu, cpu_merge=True, cpu_relocation=False)

# ---- limit gpu memory ----

def limit_gpu_memory():
    import tensorflow as tf
    from keras.backend.tensorflow_backend import set_session
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True  # dynamically grow the memory used on the GPU
    #config.log_device_placement = True  # to log device placement (on which device the operation ran)
    #                                    # (nothing gets printed in Jupyter, only if you run it standalone)
    sess = tf.Session(config=config)
    set_session(sess)  # set this TensorFlow session as the default session for Keras

# ---- callbacks ----

def checkpointer(filename:str):
    from keras import callbacks 
    return callbacks.ModelCheckpoint(filename, monitor='val_loss', 
                                            verbose=1, 
                                            save_best_only=True, 
                                            save_weights_only=False, 
                                            mode='auto', period=1)

def early_stopping(patience:int=5, monitor='val_loss'):
    from keras import callbacks 
    return callbacks.EarlyStopping(monitor=monitor, min_delta=0, patience=patience, 
                                    verbose=1, mode='auto', baseline=None, restore_best_weights=True)

def reduce_learning_rate(factor:float=0.5, patience:int=3):
    from keras import callbacks 
    return callbacks.ReduceLROnPlateau(monitor='val_loss', factor=factor, 
                                        patience=patience, verbose=1, mode='auto', min_delta=0.0001, cooldown=0, 
                                        min_lr=1e-6)

def tensorboard(histogram_freq:int=1):
    from keras import callbacks 
    return callbacks.TensorBoard(log_dir='../output/logs', histogram_freq=histogram_freq, batch_size=256, write_graph=True, write_grads=False, write_images=False, update_freq='batch')


# --- saliency map ----


def saliency_map(model, generator, image, background_batches=2):
    
    import shap
    import numpy as np

    # select a set of background examples to take an expectation over
    # this takes the mean over a number of batches of inputs images
    background = np.mean(np.array([np.mean(generator.__getitem__(0)[0], axis=0) for _ in range(background_batches)]), axis=0)

    # explain predictions
    e = shap.DeepExplainer(model, background[np.newaxis, ...])
    shap_values = e.shap_values(image[np.newaxis, ...])[0][0]

    return shap_values
            
