## Links

Awesome online book on interpretable machine learning (also LIME):

[https://christophm.github.io/interpretable-ml-book/]()

Easy-peasy Intro to LSTM:

[https://colah.github.io/posts/2015-08-Understanding-LSTMs/]()